<?php
require  'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use MyApi\Controllers\CategoriaController;
use MyApi\Controllers\PedidoController;

date_default_timezone_set('America/Sao_Paulo');

$app = new \Slim\App;


$app->get('/', function(Request $request,
                        Response $response,
                        array  $args){
    $resp = array(
        "status" => "sucesso",
        "code" => 200,
        "messages" => "Requisição atendida com sucesso",
        "data" => array(
            "api" => "SoDeCenoura API",
            "versao" => "1.0.0",
            "autor" => array(
                "ra" => "12345",
                "nome" => "J. A. Matioli"
            )
        )
    );
    return $response->withJson($resp,200);
});

$app->get('/categoria',
    function(Request $request,
             Response $response,
             array  $args) {
    $categoriaController = new CategoriaController();
    $resp = $categoriaController->get();
        return $response->withJson($resp,$resp["code"]);
});


$app->post('/categoria', function(Request $request,
                                  Response $response,
                                  array  $args){

    $parsedBody = $request->getParsedBody();
    $categoriaController = new CategoriaController();
    $resp = $categoriaController->create($parsedBody);
    return $response->withJson($resp,$resp["code"]);
});


$app->delete('/categoria/{id}', function(Request $request,
                                         Response $response,
                                         array  $args){
    $categoriaController = new CategoriaController();
    $resp = $categoriaController->delete($args['id']);
    return $response->withJson($resp,$resp["code"]);
});

$app->put('/categoria/{id}', function(Request $request,
                                         Response $response,
                                         array  $args){
    $categoriaController = new CategoriaController();
    $parsedBody = $request->getParsedBody();
    $resp = $categoriaController->update($args['id'], $parsedBody);
    return $response->withJson($resp,$resp["code"]);
});

$app->post('/pedido', function(Request $request,
                                  Response $response,
                                  array  $args){

    $parsedBody = $request->getParsedBody();
    $pedidoController = new PedidoController();
    $resp = $pedidoController->create($parsedBody);
    return $response->withJson($resp,$resp["code"]);
});

$app->get('/pedido',
    function(Request $request,
             Response $response,
             array  $args) {
        $pedidoController = new PedidoController();
        $resp = $pedidoController->get();
        return $response->withJson($resp,$resp["code"]);
    });



//CORS
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers','X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods','GET, POST, PUT, DELETE, PATCH, OPTIONS')
        ->withHeader('Content-Type','application/json');
});
$app->run();