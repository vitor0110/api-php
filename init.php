<?php
require  'vendor/autoload.php';

use MyApi\Core\Db;
use MyApi\Utils\Config;

$config = Config::getInstance();
$dbConfig = $config->get('db');
//var_dump($dbConfig);

$db = Db::getInstance();
$rows = $db->query('SELECT * FROM users ORDER BY name');
foreach ($rows as $row) {
    //var_dump($row);
    echo $row["name"]."<br>";
}

//Usando "heredoc" string
$query = <<<SQL
INSERT INTO users (name)
VALUES ("Maria Xikinha")
SQL;
$result = $db->exec($query);
//var_dump($result); // true


//Usando PREPARED STATEMENT
$query = 'SELECT * FROM users WHERE name LIKE :nome';
$statement = $db->prepare($query);
$statement->bindValue('nome', 'Maria%');
$statement->execute();
$rows = $statement->fetchAll();
//var_dump($rows);
echo "Registros encontrados:<br>";
foreach ($rows as $row) {
    //var_dump($row);
    echo $row["name"]."<br>";
}

