<?php
namespace MyApi\Controllers;
use MyApi\Models\CategoriaModel;

class CategoriaController
{
    private $catModel;
    public function __construct()
    {
        $this->catModel = new CategoriaModel();
    }

    public function get(){
        $resposta = array();
        $itens = $this->catModel->getCategorias();
        $resposta["status"] = "sucesso";
        $resposta["code"] = 200;
        $resposta["messages"] = "Requisição atendida com sucesso";
        $resposta["data"]=$itens;
        return $resposta;
    }

    public function create($dados){
        $resposta = array();
        $status="sucesso";
        $code=201;
        $msg = "Categoria criada com sucesso";
        if(!$this->catModel->criaCategoria($dados))
        {
            $status="falha";
            $code=500;
            $msg = "Falha ao gravar os dados!";
        }
        $resposta["status"] = $status;
        $resposta["code"] = $code;
        $resposta["messages"] = $msg;
        $resposta["data"] = $dados;
        return $resposta;
    }

    public function delete($idCategoria){
        $resposta = array();
        $status="sucesso";
        $code=201;
        $msg = "Categoria excluida com sucesso";
        $deletadas=array();
        if(!($deletadas=$this->catModel->deleteCategoria($idCategoria)))
        {
            $status="falha";
            $code=500;
            $msg = "Falta ID da categoria!";
        }
        $resposta["status"] = $status;
        $resposta["code"] = $code;
        $resposta["messages"] = $msg;
        $resposta["data"] = $deletadas;
        return $resposta;
    }

}