<?php
/**
 * Created by PhpStorm.
 * User: Desktop
 * Date: 28/05/2018
 * Time: 23:51
 */

namespace MyApi\Controllers;

use MyApi\Models\PedidoModel;

class PedidoController
{
    private $pedidoModel;
    public function __construct()
    {
        $this->pedidoModel = new PedidoModel();
    }

    public function get(){
        $resposta = array();
        $itens = $this->pedidoModel->getPedidos();
        $resposta["status"] = "sucesso";
        $resposta["code"] = 200;
        $resposta["messages"] = "Requisição atendida com sucesso";
        $resposta["data"]=$itens;
        return $resposta;
    }

    public function create($dados){
        $resposta = array();
        $status="sucesso";
        $code=201;
        $msg = "Pedido enviado com sucesso";
        $pedido=$this->pedidoModel->criaPedido($dados);
        if(!$pedido)
        {
            $status="falha";
            $code=500;
            $msg = "Falha ao gravar os dados!";
        }
        $resposta["status"] = $status;
        $resposta["code"] = $code;
        $resposta["messages"] = $msg;
        $resposta["data"] = $pedido;
        return $resposta;
    }


}