<?php
/**
 * Created by PhpStorm.
 * User: Desktop
 * Date: 10/07/2018
 * Time: 23:50
 */

namespace MyApi\Core;

use MyApi\Utils\Config;

use PDO;
class Db {
    private static $instance;
    private static function connect(): PDO {
        $dbConfig = Config::getInstance()->get('db');
        $db = new PDO(
            'mysql:host=143.106.241.1;dbname='.$dbConfig['dbName'],
            $dbConfig['user'],
            $dbConfig['password']
        );
        $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $db;
    }
    public static function getInstance(){
        if (self::$instance == null) {
            self::$instance = self::connect();
        }
        return self::$instance;
    }
}