<?php
namespace MyApi\Models;

class CategoriaModel
{
    private $arqDados;
    function __construct()
    {
        $this->arqDados = "dados/categorias.dat";
    }

    public function criaCategoria($categoria){
                //tenta abrir o arquivo para gravação
        if($arqCategoria=fopen($this->arqDados, "a"))
        {
                //codifica o array com os dados da categoria
                //para o formato JSON
                $jsonCategorias = json_encode($categoria)."\n";
                //retorna o resultado da tentativa de gravação
                //da categoria no arquivo
                return fwrite($arqCategoria,
                    $jsonCategorias,
                    strlen($jsonCategorias));
        }
        return false;
    }

    public function getCategorias()
    {
        $categorias=array();
        //se o arquivo existir
        if($arqCategoria=@fopen($this->arqDados, "r"))
        {
            //le o conteudo do arquivo linha a linha
            while (($linha = fgets($arqCategoria, 4096)) !== false) {
                //insere cada linha em um array do PHP,
                //decodificado de JSON para Array PHP.
                $aLinha = json_decode($linha);
                $categorias[]=$aLinha;
            }
        }
        //retorna um array com todas as categorias lidas do arquivo
        return $categorias;
    }

    public function deleteCategoria($idDeletar)
    {
        $categorias=array();
        $deletar = array();
        echo "Deletar id : ".$idDeletar;exit(0);
        //se o arquivo existir
        if($arqCategoria=fopen($this->arqDados, "r"))
        {
            //le o conteudo do arquivo linha a linha
            while (($linha = fgets($arqCategoria, 4096)) !== false)
            {
                //insere cada linha em um array do PHP,
                //decodificado de JSON para Array PHP.
                $aLinha = json_decode($linha);
                if ($aLinha->idCategoria != $idDeletar)
                    $categorias[] = $aLinha;
                else
                    $deletar[]=$aLinha;
            }
            fclose($arqCategoria);
            if ($arqCategoria = @fopen($this->arqDados, "w"))
            {
                //grava os elementos da lista $categorias
                //no arquivo
                foreach ($categorias as $categoria){
                    $jsonCategorias = json_encode($categoria)."\n";
                    fwrite($arqCategoria,
                        $jsonCategorias,
                        strlen($jsonCategorias));
                }
            }
            fclose($arqCategoria);
        }
        //retorna um array com todas as categorias lidas do arquivo
        return $deletar;
    }

}