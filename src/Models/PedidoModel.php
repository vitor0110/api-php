<?php
/**
 * Created by PhpStorm.
 * User: Desktop
 * Date: 28/05/2018
 * Time: 23:49
 */

namespace MyApi\Models;


class PedidoModel
{
    private $arqDados;
    function __construct()
    {
        $this->arqDados = "dados/pedidos.dat";
    }

    public function criaPedido($pedido){
        //tenta abrir o arquivo para gravação
        if($arqPedido=fopen($this->arqDados, "a"))
        {
            //codifica o array com os dados do pedido
            //para o formato JSON
            $pedido['status']=false;
            $pedido['data'] =  date("d-m-Y"); //data do pedido
            $pedido['hora'] =  date("H:i:s"); //hora do pedido
            //var_dump($pedido);exit(0);
            $jsonPedido = json_encode($pedido)."\n";
            //retorna o resultado da tentativa de gravação
            //do pedido no arquivo
            if(fwrite($arqPedido,$jsonPedido,strlen($jsonPedido))){
                return $pedido;
            }
            return false;
        }
        return false;
    }

    public function getPedidos()
    {
        $pedidos=array();
        //se o arquivo existir
        if($arqPedidos=@fopen($this->arqDados, "r"))
        {
            //le o conteudo do arquivo linha a linha
            while (($linha = fgets($arqPedidos, 4096)) !== false) {
                //insere cada linha em um array do PHP,
                //decodificado de JSON para Array PHP.
                $aLinha = json_decode($linha);
                $pedidos[]=$aLinha;
            }
        }
        //retorna um array com todas as categorias lidas do arquivo
        return $pedidos;
    }


}